import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styleUrls: ['./basicos.component.css']
})
export class BasicosComponent implements OnInit {
  fecha :Date= new Date();
  nombres:string='jaVier la cRuz';

  constructor() { }

  ngOnInit(): void {
  }

}
